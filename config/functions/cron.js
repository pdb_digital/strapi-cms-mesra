'use strict';

/**
 * Cron config that gives you an opportunity
 * to run scheduled jobs.
 *
 * The cron format consists of:
 * [SECOND (optional)] [MINUTE] [HOUR] [DAY OF MONTH] [MONTH OF YEAR] [DAY OF WEEK]
 *
 * See more details here: https://strapi.io/documentation/developer-docs/latest/setup-deployment-guides/configurations.html#cron-tasks
 */

module.exports = {

  // CRON Timing --> Runs at 2AM every Monday
  // "* 2 * * 1": async () => {

  //   function call for pulling products
  //   strapi.config.functions.products();

  //   function call for pulling promotions
  //   strapi.config.functions.promotions();

  // },
  
};
