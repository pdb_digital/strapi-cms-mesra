'use strict';
const axios = require('axios');
const { default: createStrapi } = require('strapi');
const { prependListener } = require('strapi-utils/lib/logger');

var limitProduct = 50;
var apiProductPage = 3;

// Configure Sentinel's Payload API Endpoint with Bearer Token
var configProduct = {
  method: 'get',
  url:
    'http://api.sentinel.getslurp.com/pricebook/538/product?id=9&site_id=RYB0764&limit=' +
    limitProduct,
  headers: {
    Authorization:
      'Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6IjYyYzdmZDRmZDcyZTkyOGY3YTA5NmUyOTM3MjI5YTc2MGJhMWZiY2M0YjZiNjg2YWNjODExNmEzNTI1ZWMwN2M2YjZlYzE0ZGI0OWU1NzQyIn0.eyJhdWQiOiIwZjkwNjYxOS04YjZjLTRlZTEtOTYzYi1hMWJlMjE0YTE2ZjgiLCJqdGkiOiI2MmM3ZmQ0ZmQ3MmU5MjhmN2EwOTZlMjkzNzIyOWE3NjBiYTFmYmNjNGI2YjY4NmFjYzgxMTZhMzUyNWVjMDdjNmI2ZWMxNGRiNDllNTc0MiIsImlhdCI6MTYzNDE4NTkyOCwibmJmIjoxNjM0MTg1OTI4LCJleHAiOjE2NjU3MjE5MjgsInN1YiI6IjI1Iiwic2NvcGVzIjpbInJlYWQ6b3V0bGV0Il0sImFjYyI6eyJmIjoxfX0.ZH_tULpmdSdI9gH6CMt6NWW9IrSeWNTF4rt2BVcs9et5gsMY8CFDY5NrV9QH4W6ddzpESBjwfP0nxiRFcHxz0ZQvLs_jaliQfzmhiXCQx1Ot0Wx44wIGII3HXT27JbkfiNd7R8MRzUdgFJSHPv-gt0qvNK94fUHQVrlzLzXuOKtYXK5XbI_VOPB5KjXjWjQqI_9T-hKiXkKTBrJvfdkixrJozg3IXPvA6kNZzpB-yXa7lGxUcFDbFrC9ZYhMi23UZ0byGcyWSjqcozUwMxWv4j_pdt99yASZ1rFjf0fa9O5cxJt1M5S7L9Ss0Co-XQgl_10CQJUDaMiY7RkevbGoV7dsrBNgGtRj9YYSrb9gEYovphp8lcsDC3CZI0FzQaB4Z7UhDO29js3B7TTU3Iv7G26Rlaonzi0axKoz_eVGZe5L2ysAK8W9pC-wUl0wBUPQ0aXUzRBE6jJiGD_Pb2ZIqGFPLXPB6f2_rFyt0ayzFCPpx8Rn4HpbqeiCP3eJegI9m57OEWJsDWuro6te-0vkv4hqVpJkUKCer8oeoqKJ_BTJePne-ME3OG5l7HgpvtGUCRXIqM3-hVhAbt_803Irux-MZf7Y9ouBx-nD4B7g7c6hE8wDCOTAB0IvavnxHVGDZwG5i8Wt7DGV3I1zQHi_WGvF6K2fWL4YeCWM3mqToIQ',
  },
};

// Product Data Pulling Logic
module.exports = async () => {

    let firstCheck = await strapi.query("product").count();

    if (firstCheck == 0) {

        // Insert all products from sentinel into strapi for the first time 
        for (let currentPage = 1; currentPage < apiProductPage; currentPage++) {

            // Function that return all products per page
            let result = await sentinelProductQuery(currentPage);

            //To loop through all products per page
            for (let prodNum = 0; prodNum < limitProduct; prodNum++) {
                let sentinelProd = result.data[prodNum];
                await strapi.query("product").create({
                    code: sentinelProd.code,
                    name: sentinelProd.name,
                });
                
                // console.log(sentinelProd.name + " added to product model!");
            }
        }   
    } else {

    // Update inserted products in strapi with newer data from sentinel (if applicable)

    // To check the sentinel api per page
    for (let currentPage = 1; currentPage < apiProductPage; currentPage++) {

    // Function that return all products per page
    let result = await sentinelProductQuery(currentPage);

    // To loop thorugh all products in the current page
    for (let prodNum = 0; prodNum < limitProduct; prodNum++) {
        let strapiProd = await strapi
            .query("product")
            .findOne({ code: result.data[prodNum].code });
        let sentinelProdDate = new Date(result.data[prodNum].updated_at);
        let strapiProdDate = new Date(strapiProd.updated_at);

        if (
        sentinelProdDate.getFullYear() >= strapiProdDate.getFullYear() &&
        sentinelProdDate.getMonth() >= strapiProdDate.getMonth() &&
        sentinelProdDate.getDate() >= strapiProdDate.getDate()
        ) {
        /**
         * strapi query update with the latest data from sentinel
         */        
        console.log(strapiProd.name + " year >= sentinel's");
      }
    }
  }
}

    async function sentinelProductQuery(apiPage) {

        configProduct.url = configProduct.url + "&page=" + apiPage;
        let result = await axios(configProduct);
    
        // To reset the url to the base url without page param
        configProduct.url = 
        'http://api.sentinel.getslurp.com/pricebook/538/product?id=9&site_id=RYB0764&limit=' + limitProduct;
    
        return result;
    }

};