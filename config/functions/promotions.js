'use strict';
const axios = require('axios');
const { default: createStrapi } = require('strapi');
const { prependListener } = require('strapi-utils/lib/logger');

var limitPromo = 15;
var apiPromoPage = 46;

var configPromo = {
  method: 'get',
  url: 'http://api.sentinel.getslurp.com/promotion?site_id=RYB0764&limit=' + limitPromo,
  headers: {
    Authorization:
      'Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6IjYyYzdmZDRmZDcyZTkyOGY3YTA5NmUyOTM3MjI5YTc2MGJhMWZiY2M0YjZiNjg2YWNjODExNmEzNTI1ZWMwN2M2YjZlYzE0ZGI0OWU1NzQyIn0.eyJhdWQiOiIwZjkwNjYxOS04YjZjLTRlZTEtOTYzYi1hMWJlMjE0YTE2ZjgiLCJqdGkiOiI2MmM3ZmQ0ZmQ3MmU5MjhmN2EwOTZlMjkzNzIyOWE3NjBiYTFmYmNjNGI2YjY4NmFjYzgxMTZhMzUyNWVjMDdjNmI2ZWMxNGRiNDllNTc0MiIsImlhdCI6MTYzNDE4NTkyOCwibmJmIjoxNjM0MTg1OTI4LCJleHAiOjE2NjU3MjE5MjgsInN1YiI6IjI1Iiwic2NvcGVzIjpbInJlYWQ6b3V0bGV0Il0sImFjYyI6eyJmIjoxfX0.ZH_tULpmdSdI9gH6CMt6NWW9IrSeWNTF4rt2BVcs9et5gsMY8CFDY5NrV9QH4W6ddzpESBjwfP0nxiRFcHxz0ZQvLs_jaliQfzmhiXCQx1Ot0Wx44wIGII3HXT27JbkfiNd7R8MRzUdgFJSHPv-gt0qvNK94fUHQVrlzLzXuOKtYXK5XbI_VOPB5KjXjWjQqI_9T-hKiXkKTBrJvfdkixrJozg3IXPvA6kNZzpB-yXa7lGxUcFDbFrC9ZYhMi23UZ0byGcyWSjqcozUwMxWv4j_pdt99yASZ1rFjf0fa9O5cxJt1M5S7L9Ss0Co-XQgl_10CQJUDaMiY7RkevbGoV7dsrBNgGtRj9YYSrb9gEYovphp8lcsDC3CZI0FzQaB4Z7UhDO29js3B7TTU3Iv7G26Rlaonzi0axKoz_eVGZe5L2ysAK8W9pC-wUl0wBUPQ0aXUzRBE6jJiGD_Pb2ZIqGFPLXPB6f2_rFyt0ayzFCPpx8Rn4HpbqeiCP3eJegI9m57OEWJsDWuro6te-0vkv4hqVpJkUKCer8oeoqKJ_BTJePne-ME3OG5l7HgpvtGUCRXIqM3-hVhAbt_803Irux-MZf7Y9ouBx-nD4B7g7c6hE8wDCOTAB0IvavnxHVGDZwG5i8Wt7DGV3I1zQHi_WGvF6K2fWL4YeCWM3mqToIQ',
  },
};

// Promo Data Pulling Logic
module.exports = async () => {

    let firstCheck = await strapi.query("promotion").count();

    // If promotion collection is empty
    if (firstCheck == 0) {

        // Insert all promo from sentinel into strapi for the first time 
        for (let currentPage = 1; currentPage < apiPromoPage; currentPage++) {

            // Function that return all promotions per page
            let result = await sentinelPromoQuery(currentPage);

            //To loop through all promotions per page
            for (let promoNum = 0; promoNum < limitPromo; promoNum++) {
                let sentinelPromo = result.data[promoNum];
                await strapi.query("promotion").create({
                    id: sentinelPromo.promo_id,
                    name: sentinelPromo.name,
                    type: sentinelPromo.type,
                    available_from: sentinelPromo.available_from,
                    available_to: sentinelPromo.available_to,
                    // trigger_conditions[] sentinelPromo.products.product_id,
                    // apply_actions.price: sentinelPromo.final_price,
                });
            }
        }   

    } else {

    // Update inserted promotions in strapi with newer data from sentinel (if applicable)

    // To check the sentinel api per page
    for (let currentPage = 1; currentPage < apiPromoPage; currentPage++) {

    // Function that return all promo per page
    let result = await sentinelPromoQuery(currentPage);

    // To loop thorugh all promo in the current page
    for (let promoNum = 0; promoNum < limitPromo; promoNum++) {
        let strapiPromo = await strapi
            .query("promotion")
            .findOne({ code: result.data[promoNum].code });
        let sentinelPromoDate = new Date(result.data[promoNum].updated_at);
        let strapiPromoDate = new Date(strapiPromo.updated_at);
        if (
        sentinelPromoDate.getFullYear() >= strapiPromoDate.getFullYear() &&
        sentinelPromoDate.getMonth() >= strapiPromoDate.getMonth() &&
        sentinelPromoDate.getDate() >= strapiPromoDate.getDate()
        ) {
        /**
         * strapi query update with the latest data from sentinel
         */
        console.log(strapiPromo.name + " year >= sentinel's");
      }
    }
  }
}

    async function sentinelPromotionQuery(apiPage) {

        // configPromo.url = configPromo.url + "&page=" + apiPage;
        // let result = await axios(configPromo);
    
        // // To reset the url to the base url without page param
        // configPromo.url = 
        // 'http://api.sentinel.getslurp.com/pricebook/538/product?id=9&site_id=RYB0764&limit=' + limitPromo;
    
        // return result;
    }

};