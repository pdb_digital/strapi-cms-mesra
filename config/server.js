module.exports = ({ env }) => ({
  host: env('HOST', '0.0.0.0'),
  port: env.int('PORT', 1337),
  admin: {
    auth: {
      secret: env('ADMIN_JWT_SECRET', '83e9708a88ee1b08c9919a1613433079'),
    },
  },
  // cron: {
  //   enabled: true,
  // },
});
